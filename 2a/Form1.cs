﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2a
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textB_op1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_zbroj_Click(object sender, EventArgs e)
        {
            double op1, op2;
            if (!double.TryParse(textB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1");
            else if (!double.TryParse(textB_op2.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2");
            else
                lbl_rez.Text = (op1 + op2).ToString();
        }

        private void btn_razlika_Click(object sender, EventArgs e)
        {
            double op1, op2;
            if (!double.TryParse(textB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1");
            else if (!double.TryParse(textB_op2.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2");
            else
                lbl_rez.Text = (op1 - op2).ToString();
        }

        private void btn_umnozak_Click(object sender, EventArgs e)
        {
            double op1, op2;
            if (!double.TryParse(textB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1");
            else if (!double.TryParse(textB_op2.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2");
            else
                lbl_rez.Text = (op1 * op2).ToString();
        }

        private void btn_kvocijent_Click(object sender, EventArgs e)
        {
            double op1, op2;
            if (!double.TryParse(textB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1");
            else if (!double.TryParse(textB_op2.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2");
            else
                lbl_rez.Text = (op1 / op2).ToString();
        }

        private void btn_sqrt_Click(object sender, EventArgs e)
        {
            double op1, op2;
            if (!double.TryParse(textB_op1.Text, out op1) || op1 < 0)
                MessageBox.Show("Pogresan unos operanda 1");
            else if (!double.TryParse(textB_op2.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2");
            else
                lbl_rez.Text = (Math.Pow(op1, 1/op2)).ToString();
        }

        private void btn_pow_Click(object sender, EventArgs e)
        {
            double op1, op2;
            if (!double.TryParse(textB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1");
            else if (!double.TryParse(textB_op2.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2");
            else
                lbl_rez.Text = (Math.Pow(op1, op2)).ToString();
        }

        private void btn_sin_Click(object sender, EventArgs e)
        {
            double op1;
            if (!double.TryParse(textB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1");
            else
                lbl_rez.Text = (Math.Sin(op1)).ToString();
        }

        private void btn_cos_Click(object sender, EventArgs e)
        {
            double op1;
            if (!double.TryParse(textB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1");
            else
                lbl_rez.Text = (Math.Cos(op1)).ToString();
        }

        private void btn_tan_Click(object sender, EventArgs e)
        {
            double op1;
            if (!double.TryParse(textB_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1");
            else
                lbl_rez.Text = (Math.Tan(op1)).ToString();
        }

        private void btn_izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
