﻿namespace _2a
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_op1 = new System.Windows.Forms.Label();
            this.lbl_op2 = new System.Windows.Forms.Label();
            this.textB_op1 = new System.Windows.Forms.TextBox();
            this.textB_op2 = new System.Windows.Forms.TextBox();
            this.lbl_rez = new System.Windows.Forms.Label();
            this.btn_zbroj = new System.Windows.Forms.Button();
            this.btn_razlika = new System.Windows.Forms.Button();
            this.btn_umnozak = new System.Windows.Forms.Button();
            this.btn_kvocijent = new System.Windows.Forms.Button();
            this.btn_root = new System.Windows.Forms.Button();
            this.btn_pow = new System.Windows.Forms.Button();
            this.btn_sin = new System.Windows.Forms.Button();
            this.btn_cos = new System.Windows.Forms.Button();
            this.btn_tan = new System.Windows.Forms.Button();
            this.btn_izlaz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_op1
            // 
            this.lbl_op1.AutoSize = true;
            this.lbl_op1.Location = new System.Drawing.Point(39, 46);
            this.lbl_op1.Name = "lbl_op1";
            this.lbl_op1.Size = new System.Drawing.Size(60, 13);
            this.lbl_op1.TabIndex = 0;
            this.lbl_op1.Text = "Operand 1:";
            // 
            // lbl_op2
            // 
            this.lbl_op2.AutoSize = true;
            this.lbl_op2.Location = new System.Drawing.Point(39, 92);
            this.lbl_op2.Name = "lbl_op2";
            this.lbl_op2.Size = new System.Drawing.Size(60, 13);
            this.lbl_op2.TabIndex = 1;
            this.lbl_op2.Text = "Operand 2:";
            // 
            // textB_op1
            // 
            this.textB_op1.Location = new System.Drawing.Point(145, 46);
            this.textB_op1.Name = "textB_op1";
            this.textB_op1.Size = new System.Drawing.Size(100, 20);
            this.textB_op1.TabIndex = 2;
            this.textB_op1.TextChanged += new System.EventHandler(this.textB_op1_TextChanged);
            // 
            // textB_op2
            // 
            this.textB_op2.Location = new System.Drawing.Point(145, 92);
            this.textB_op2.Name = "textB_op2";
            this.textB_op2.Size = new System.Drawing.Size(100, 20);
            this.textB_op2.TabIndex = 3;
            // 
            // lbl_rez
            // 
            this.lbl_rez.AutoSize = true;
            this.lbl_rez.Location = new System.Drawing.Point(303, 73);
            this.lbl_rez.Name = "lbl_rez";
            this.lbl_rez.Size = new System.Drawing.Size(46, 13);
            this.lbl_rez.TabIndex = 4;
            this.lbl_rez.Text = "Rezultat";
            // 
            // btn_zbroj
            // 
            this.btn_zbroj.Location = new System.Drawing.Point(42, 171);
            this.btn_zbroj.Name = "btn_zbroj";
            this.btn_zbroj.Size = new System.Drawing.Size(75, 23);
            this.btn_zbroj.TabIndex = 5;
            this.btn_zbroj.Text = "+";
            this.btn_zbroj.UseVisualStyleBackColor = true;
            this.btn_zbroj.Click += new System.EventHandler(this.btn_zbroj_Click);
            // 
            // btn_razlika
            // 
            this.btn_razlika.Location = new System.Drawing.Point(145, 171);
            this.btn_razlika.Name = "btn_razlika";
            this.btn_razlika.Size = new System.Drawing.Size(75, 23);
            this.btn_razlika.TabIndex = 6;
            this.btn_razlika.Text = "-";
            this.btn_razlika.UseVisualStyleBackColor = true;
            this.btn_razlika.Click += new System.EventHandler(this.btn_razlika_Click);
            // 
            // btn_umnozak
            // 
            this.btn_umnozak.Location = new System.Drawing.Point(253, 170);
            this.btn_umnozak.Name = "btn_umnozak";
            this.btn_umnozak.Size = new System.Drawing.Size(75, 23);
            this.btn_umnozak.TabIndex = 7;
            this.btn_umnozak.Text = "*";
            this.btn_umnozak.UseVisualStyleBackColor = true;
            this.btn_umnozak.Click += new System.EventHandler(this.btn_umnozak_Click);
            // 
            // btn_kvocijent
            // 
            this.btn_kvocijent.Location = new System.Drawing.Point(42, 222);
            this.btn_kvocijent.Name = "btn_kvocijent";
            this.btn_kvocijent.Size = new System.Drawing.Size(75, 23);
            this.btn_kvocijent.TabIndex = 8;
            this.btn_kvocijent.Text = "/";
            this.btn_kvocijent.UseVisualStyleBackColor = true;
            this.btn_kvocijent.Click += new System.EventHandler(this.btn_kvocijent_Click);
            // 
            // btn_root
            // 
            this.btn_root.Location = new System.Drawing.Point(145, 221);
            this.btn_root.Name = "btn_root";
            this.btn_root.Size = new System.Drawing.Size(75, 23);
            this.btn_root.TabIndex = 9;
            this.btn_root.Text = "root";
            this.btn_root.UseVisualStyleBackColor = true;
            this.btn_root.Click += new System.EventHandler(this.btn_sqrt_Click);
            // 
            // btn_pow
            // 
            this.btn_pow.Location = new System.Drawing.Point(253, 221);
            this.btn_pow.Name = "btn_pow";
            this.btn_pow.Size = new System.Drawing.Size(75, 23);
            this.btn_pow.TabIndex = 10;
            this.btn_pow.Text = "pow";
            this.btn_pow.UseVisualStyleBackColor = true;
            this.btn_pow.Click += new System.EventHandler(this.btn_pow_Click);
            // 
            // btn_sin
            // 
            this.btn_sin.Location = new System.Drawing.Point(42, 277);
            this.btn_sin.Name = "btn_sin";
            this.btn_sin.Size = new System.Drawing.Size(75, 23);
            this.btn_sin.TabIndex = 11;
            this.btn_sin.Text = "sin";
            this.btn_sin.UseVisualStyleBackColor = true;
            this.btn_sin.Click += new System.EventHandler(this.btn_sin_Click);
            // 
            // btn_cos
            // 
            this.btn_cos.Location = new System.Drawing.Point(145, 276);
            this.btn_cos.Name = "btn_cos";
            this.btn_cos.Size = new System.Drawing.Size(75, 23);
            this.btn_cos.TabIndex = 12;
            this.btn_cos.Text = "cos";
            this.btn_cos.UseVisualStyleBackColor = true;
            this.btn_cos.Click += new System.EventHandler(this.btn_cos_Click);
            // 
            // btn_tan
            // 
            this.btn_tan.Location = new System.Drawing.Point(253, 275);
            this.btn_tan.Name = "btn_tan";
            this.btn_tan.Size = new System.Drawing.Size(75, 23);
            this.btn_tan.TabIndex = 13;
            this.btn_tan.Text = "tan";
            this.btn_tan.UseVisualStyleBackColor = true;
            this.btn_tan.Click += new System.EventHandler(this.btn_tan_Click);
            // 
            // btn_izlaz
            // 
            this.btn_izlaz.Location = new System.Drawing.Point(365, 321);
            this.btn_izlaz.Name = "btn_izlaz";
            this.btn_izlaz.Size = new System.Drawing.Size(75, 23);
            this.btn_izlaz.TabIndex = 14;
            this.btn_izlaz.Text = "Izlaz";
            this.btn_izlaz.UseVisualStyleBackColor = true;
            this.btn_izlaz.Click += new System.EventHandler(this.btn_izlaz_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 356);
            this.Controls.Add(this.btn_izlaz);
            this.Controls.Add(this.btn_tan);
            this.Controls.Add(this.btn_cos);
            this.Controls.Add(this.btn_sin);
            this.Controls.Add(this.btn_pow);
            this.Controls.Add(this.btn_root);
            this.Controls.Add(this.btn_kvocijent);
            this.Controls.Add(this.btn_umnozak);
            this.Controls.Add(this.btn_razlika);
            this.Controls.Add(this.btn_zbroj);
            this.Controls.Add(this.lbl_rez);
            this.Controls.Add(this.textB_op2);
            this.Controls.Add(this.textB_op1);
            this.Controls.Add(this.lbl_op2);
            this.Controls.Add(this.lbl_op1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_op1;
        private System.Windows.Forms.Label lbl_op2;
        private System.Windows.Forms.TextBox textB_op1;
        private System.Windows.Forms.TextBox textB_op2;
        private System.Windows.Forms.Label lbl_rez;
        private System.Windows.Forms.Button btn_zbroj;
        private System.Windows.Forms.Button btn_razlika;
        private System.Windows.Forms.Button btn_umnozak;
        private System.Windows.Forms.Button btn_kvocijent;
        private System.Windows.Forms.Button btn_root;
        private System.Windows.Forms.Button btn_pow;
        private System.Windows.Forms.Button btn_sin;
        private System.Windows.Forms.Button btn_cos;
        private System.Windows.Forms.Button btn_tan;
        private System.Windows.Forms.Button btn_izlaz;
    }
}

